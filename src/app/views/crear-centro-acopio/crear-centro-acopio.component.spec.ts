import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CrearCentroAcopioComponent } from './crear-centro-acopio.component';

describe('CrearCentroAcopioComponent', () => {
  let component: CrearCentroAcopioComponent;
  let fixture: ComponentFixture<CrearCentroAcopioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrearCentroAcopioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrearCentroAcopioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
