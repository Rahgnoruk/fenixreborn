import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {Http} from '@angular/http';
import {HttpClient} from '@angular/common/http';
import {HttpErrorResponse} from '@angular/common/http';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss']
})
export class SignInComponent implements OnInit {
  usuarios: string[];
  constructor(private router: Router, private httpService: HttpClient) { }

  ngOnInit() {
    this.httpService.get("../../assets/jsons/CuentasUsuario.json").subscribe(
      data => {
        this.usuarios = data as string[];
      },
      (err: HttpErrorResponse) => {
        console.log(err.message);
      }
    );
  }

  donador(){
    this.router.navigate(["ver-centros-mapa"]);
  }
  administrarCentro(){
    this.router.navigate(["administrar-centro-acopio"]);
  }
  crearCentro(){
    this.router.navigate(["crear-centro-acopio"]);
  }

}
