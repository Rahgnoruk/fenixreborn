import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VerCentrosMapaComponent } from './ver-centros-mapa.component';

describe('VerCentrosMapaComponent', () => {
  let component: VerCentrosMapaComponent;
  let fixture: ComponentFixture<VerCentrosMapaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VerCentrosMapaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerCentrosMapaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
