import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-ver-centros-mapa',
  templateUrl: './ver-centros-mapa.component.html',
  styleUrls: ['./ver-centros-mapa.component.scss']
})
export class VerCentrosMapaComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }
  verCentroAcopio(){
    this.router.navigate(["ver-perfil-centro-acopio"]);
  }
}
