import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VerPerfilCentroAcopioComponent } from './ver-perfil-centro-acopio.component';

describe('VerPerfilCentroAcopioComponent', () => {
  let component: VerPerfilCentroAcopioComponent;
  let fixture: ComponentFixture<VerPerfilCentroAcopioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VerPerfilCentroAcopioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerPerfilCentroAcopioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
