import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {Http} from '@angular/http';
import {HttpClient} from '@angular/common/http';
import {HttpErrorResponse} from '@angular/common/http';
import {Chart} from 'chart.js';

@Component({
  selector: 'app-ver-perfil-centro-acopio',
  templateUrl: './ver-perfil-centro-acopio.component.html',
  styleUrls: ['./ver-perfil-centro-acopio.component.scss']
})
export class VerPerfilCentroAcopioComponent implements OnInit {
  chart = [];
  public barChartOptions:any = {
    scaleShowVerticalLines: false,
    responsive: true
  }
  private centros;
  private nombreCentro: string="Acoxpa";
  private necesidades;
  private i;
  private medicamentos: {};
  private medicamento: string;
  private cantidad: string;
  private comida: {};
  private material: {};

  constructor(private router: Router, private httpService: HttpClient) {
    /*this.nombreCentro = this.centros.Centros[0].Nombre;
    this.necesidades = this.centros.Centros[0].Necesidades;
    for(this.i in this.necesidades.Medicamentos){
      this.medicamento = this.i.nombre;
      this.cantidad = this.i.cantidad;
    }
    for(let i in this.necesidades.Alimentos){

    }
    for(let i in this.necesidades.Materiales){

    }
    this.medicamentos = this.necesidades.Medicamentos;
    this.comida = this.centros[0].Comida;
    this.material = this.centros[0].Material;
  */}
  ngOnInit() {
    /*this.httpService.get('../../assets/jsons/Centros.json').subscribe(
      data => {
        console.log(data as string);
        this.centros = JSON.parse(data as string);
      },
      (err: HttpErrorResponse) => {
        console.log(err.message);
      }
    );*/
  }
  public barChartLabels:string[] = ['Ibuprofeno', 'Paracetamol', 'Enlatados', 'Agua', 'Papel Higienico', 'Cubrebocas', 'Frijoles'];
    public barChartType:string = 'bar';
    public barChartLegend:boolean = true;

    public barChartData:any[] = [
      {data: [100, 80, 120, 200, 120, 50, 80], label: 'Necesitado'},
      {data: [20, 90, 80, 400, 40, 30, 50], label: 'Existente'}
    ];

    // events
    public chartClicked(e:any):void {
      console.log(e);
    }

    public chartHovered(e:any):void {
      console.log(e);
    }
}
