import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdministrarCentroAcopioComponent } from './administrar-centro-acopio.component';

describe('AdministrarCentroAcopioComponent', () => {
  let component: AdministrarCentroAcopioComponent;
  let fixture: ComponentFixture<AdministrarCentroAcopioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdministrarCentroAcopioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdministrarCentroAcopioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
