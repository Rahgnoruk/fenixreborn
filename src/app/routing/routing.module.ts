import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { SignInComponent } from '../views/sign-in/sign-in.component';
import { AboutUsComponent } from '../views/about-us/about-us.component';
import { CrearCentroAcopioComponent } from '../views/crear-centro-acopio/crear-centro-acopio.component';
import { VerPerfilCentroAcopioComponent } from '../views/ver-perfil-centro-acopio/ver-perfil-centro-acopio.component';
import { VerCentrosMapaComponent } from '../views/ver-centros-mapa/ver-centros-mapa.component';
import { AdministrarCentroAcopioComponent } from '../views/administrar-centro-acopio/administrar-centro-acopio.component';

const routes: Routes = [
  { path: '', redirectTo: '/sign-in', pathMatch: 'full' },
  { path: 'sign-in', component: SignInComponent },
  { path: 'about-us', component: AboutUsComponent,data:{red:true} },
  { path: 'crear-centro-acopio', component: CrearCentroAcopioComponent,data:{red:true} },
  { path: 'ver-perfil-centro-acopio', component: VerPerfilCentroAcopioComponent,data:{red:true} },
  { path: 'ver-centros-mapa', component: VerCentrosMapaComponent},
  { path: 'administrar-centro-acopio', component: AdministrarCentroAcopioComponent,data:{red:true} },
  { path: '**', redirectTo: '/sign-in', pathMatch: 'prefix' },
  //{ path: 'lista-productos/:category', component: VerCentrosMapaComponent,data:{products:true} },
  //{ path: 'producto/:category/:type/:id', component: AdministrarCentroAcopioComponent,data:{product:true} },
];

@NgModule({
  imports:[
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ],
  declarations: []
})
export class AppRoutingModule { }
