import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {RouterModule,Routes} from '@angular/router';

import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { AboutUsComponent } from './views/about-us/about-us.component';
import { AdministrarCentroAcopioComponent } from './views/administrar-centro-acopio/administrar-centro-acopio.component';
import { CrearCentroAcopioComponent } from './views/crear-centro-acopio/crear-centro-acopio.component';
import { SignInComponent } from './views/sign-in/sign-in.component';
import { VerCentrosMapaComponent } from './views/ver-centros-mapa/ver-centros-mapa.component';
import { VerPerfilCentroAcopioComponent } from './views/ver-perfil-centro-acopio/ver-perfil-centro-acopio.component';
import {AppRoutingModule} from './routing/routing.module';
import { AlertModule } from 'ngx-bootstrap';
import {HttpClientModule} from "@angular/common/http";
import {ChartsModule} from 'ng2-charts';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    AboutUsComponent,
    AdministrarCentroAcopioComponent,
    CrearCentroAcopioComponent,
    SignInComponent,
    VerCentrosMapaComponent,
    VerPerfilCentroAcopioComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AlertModule,
    HttpClientModule,
    ChartsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
