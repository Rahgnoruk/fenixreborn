import { TestBed, inject } from '@angular/core/testing';

import { CrearCentroService } from './crear-centro.service';

describe('CrearCentroService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CrearCentroService]
    });
  });

  it('should be created', inject([CrearCentroService], (service: CrearCentroService) => {
    expect(service).toBeTruthy();
  }));
});
